# Projeto ToDoList com Redux

## Instale as dependências
`npm install` ou `yarn install`

## Server
`npm run server` ou `yarn server`

## App
`npm run start` ou `yarn start`

### App final

<img src="./src/assets/todo-list-redux.png" />